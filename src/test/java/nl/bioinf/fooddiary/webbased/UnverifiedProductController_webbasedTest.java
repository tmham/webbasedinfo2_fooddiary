package nl.bioinf.fooddiary.webbased;

import nl.bioinf.fooddiary.FooddiaryApplication;
import nl.bioinf.fooddiary.control.UnverifiedProductController_webbased;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= FooddiaryApplication.class)
@AutoConfigureMockMvc
class UnverifiedProductController_webbasedTest {

    @Autowired
    private MockMvc mockMvc;
    private UnverifiedProductController_webbased controller;

    @Test
    private void testingController() throws Exception {
        controllerNotNull();
        getUnverifiedProduct();
    }

    private void controllerNotNull() {
        assertThat(controller).isNotNull();
    }

    private void getUnverifiedProduct() throws Exception {
        String responseJson = "[{'id':'2','user_id':'1','date':'2020-05-13','time_of_day':'13:21','mealtime':'lunch'," +
                "'description':'Quacker crueslie','quantity':'145'}]";
        this.mockMvc.perform(get("/unverifiedProduct/2")).andExpect(status().isOk()).andExpect(content().json(responseJson));
    }

}
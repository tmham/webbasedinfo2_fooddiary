package nl.bioinf.fooddiary.control;

import nl.bioinf.fooddiary.model.newproduct.NewProduct;
import nl.bioinf.fooddiary.service.NewProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UnverifiedProductController_webbased {

    @Autowired
    NewProductService newProductService;
//
//    @GetMapping(value="/unverifiedProducts")
//    public String getUnverifiedProduct() {
//        return "I'm returning something";
//    }
    @GetMapping(value="/unverifiedProducts")
    public ResponseEntity<List<NewProduct>> unverifiedProducts() {
        return new ResponseEntity(newProductService.getAllNewProducts(), HttpStatus.OK);
    }

    @GetMapping(value= "/unverifiedProducts/{id}")
    public ResponseEntity<NewProduct> getUnverifiedProductByID(@PathVariable int id) {
        NewProduct newProductByID;
        newProductByID = newProductService.getNewProductById(id);
        if(newProductByID == null) {
            NewProduct emptyProduct = new NewProduct();
            return new ResponseEntity(emptyProduct,  HttpStatus.OK);
        }
        return new ResponseEntity(newProductByID, HttpStatus.OK);
    }
}

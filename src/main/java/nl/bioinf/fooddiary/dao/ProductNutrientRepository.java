package nl.bioinf.fooddiary.dao;


import nl.bioinf.fooddiary.model.nutrient.ProductNutrient;

public interface ProductNutrientRepository {

    int insertProductNutrientData(ProductNutrient productNutrient);

}
